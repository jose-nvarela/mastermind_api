import unittest

from fastapi.testclient import TestClient

from mastermind_api.api.app import app


class TestGuessEndpoints(unittest.TestCase):
    def setUp(self):
        # self.session = setup_database()
        self.client = TestClient(app)
        self.guess_path = "/api/v1/guess/"

    def test_post_create_guess_ok1(self):
        # create game RGGB
        response = self.client.post(
            self.guess_path,
            headers={},
            json={"game_id": 1, "code": "rggb"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            "game_id": 1,
            "code": "rggb",
            "timestamp": None,
            "id": None,
            "black_pegs": 4,
            "white_pegs": 0
        })

    def test_post_create_guess_ok2(self):
        # create game rrrr
        response = self.client.post(
            self.guess_path,
            headers={},
            json={"game_id": 1, "code": "byob"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            "game_id": 1,
            "code": "byob",
            "timestamp": None,
            "id": None,
            "black_pegs": 0,
            "white_pegs": 0
        })

    def test_post_create_guess_ok3(self):
        # create game gbbr
        response = self.client.post(
            self.guess_path,
            headers={},
            json={"game_id": 1, "code": "gbrb"},
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            "game_id": 1,
            "code": "gbrb",
            "timestamp": None,
            "id": None,
            "black_pegs": 2,
            "white_pegs": 2
        })

    def test_post_create_guess_validation_ko1(self):
        response = self.client.post(
            self.guess_path,
            headers={},
            json={"game_id": "Esto falla >.<", "code": "gbrb"},
        )
        self.assertEqual(response.status_code, 422)
