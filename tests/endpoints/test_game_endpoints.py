import unittest

from fastapi.testclient import TestClient

from mastermind_api.api.app import app


class TestGameEndpoints(unittest.TestCase):
    def setUp(self):
        # self.session = setup_database()
        self.client = TestClient(app)
        self.game_path = "/api/v1/game/"

    def test_post_create_game_ok1(self):
        # create game RGGB
        response = self.client.post(
            self.game_path,
            headers={},
            json={"game_id": 1, "code": "rggb"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            "code": "rggb",
            "state": None,
            "timestamp": None,
            "id": None
        })

    def test_get_current_game_ok1(self):
        # create game RGGB with guess
        game_id = 1
        response = self.client.get(
            self.game_path + '?game_id={}'.format(str(game_id))
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            "code": "rrgg",
            "state": None,
            "timestamp": None,
            "id": 1,
            "guess": [
                {
                    "game_id": 1,
                    "code": "rrgg",
                    "timestamp": None,
                    "id": None
                }
            ]
        })
