import unittest
from types import NotImplementedType

from pydantic.error_wrappers import ValidationError
from tests.utils.db import setup_database, create_current_game

from mastermind_api.infra.database.repositories.repositories import GameRepo

from mastermind_api.entities.game_entity import Game as GameEntity
from mastermind_api.infra.database.models import Game as GameModel


class TestGameRepository(unittest.TestCase):
    def setUp(self):
        self.session = setup_database()

    def test_create_game_ok1(self):
        game_obj = GameEntity(code='rrgg', state='on')
        game = GameRepo(self.session).create_game(game=game_obj)

        self.assertEqual(game.code, 'rrgg')

    def test_create_game_ko1(self):
        game_obj = GameEntity(code='rrgg', state='on')
        game = GameRepo(self.session).create_game(game=game_obj)

        self.assertNotEqual(game.code, 'zzzz')

    def test_create_game_ko2(self):
        with self.assertRaises(ValidationError):
            game_obj = GameEntity(code=NotImplementedType,
                                  state=NotImplementedType)
            GameRepo(self.session).create_game(game=game_obj)

    def test_get_game_ok1(self):
        game = create_current_game(self.session)
        game_res: GameEntity = GameRepo(self.session).get_game(
            game_id=game.id)

        self.assertEqual(game_res.code, game.code)

    def test_current_game_ko1(self):
        game = create_current_game(self.session)
        with self.assertRaises(Exception):
            GameRepo(self.session).get_current_game(GameEntity(
                id=879,
                code=game.code,
                state=game.state,
                timestamp=game.timestamp))

    def test_get_game_ok2(self):
        game = create_current_game(self.session)
        # change state
        game_res: GameEntity = GameRepo(self.session).update_game(
            GameEntity(id=game.id, state='fake_state', code='xxx'))
        game: GameModel = self.session.query(
            GameModel).filter_by(id=game_res.id).first()

        self.assertEqual(game.state, 'fake_state')
