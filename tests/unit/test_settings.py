import unittest

from tests.utils.config import get_config


class TestSettings(unittest.TestCase):
    def setUp(self):
        self.conf = get_config()

    def test_settings_ok1(self):

        self.assertTrue(self.conf.db_engine_url == 'fakeurl')

    def test_settings_ok2(self):

        self.assertTrue(self.conf.log_level == 'fatal')
