import unittest

from mastermind_api.use_cases.resolve_game import ResolveGame
from mastermind_api.use_cases.validate_code import (
    ValidateCode, ValidateCodeLen)
from mastermind_api.use_cases.change_state import ChangeState
from mastermind_api.config.settings import config

from mastermind_api.entities import Guess, Game


class TestSettings(unittest.TestCase):
    def setUp(self):
        pass

    def test_resolve_game_ok1(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='rggb',
                      id=1), guess=Guess(code='rggb',
                                         game_id=1)).execute()

        self.assertEqual(black_pegs, 4)
        self.assertEqual(white_pegs, 0)

    def test_resolve_game_ok2(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='rrrr', id=1),
            guess=Guess(code='byob',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 0)
        self.assertEqual(white_pegs, 0)

    def test_resolve_game_ok3(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='gbbr', id=1),
            guess=Guess(code='gbrb',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 2)
        self.assertEqual(white_pegs, 2)

    def test_resolve_game_ok4(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='bbbr'),
            guess=Guess(code='rbgg',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 1)
        self.assertEqual(white_pegs, 1)

    def test_resolve_game_ok5(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='rbgg'),
            guess=Guess(code='bbbr',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 1)
        self.assertEqual(white_pegs, 1)

    # def test_resolve_game_ok6(self):
    #     black_pegs, white_pegs = ResolveGame(
    #         game=Game(code='bbbr', id=1),
    #               guess=Guess(code='bbrb', game_id=1)).execute()
    #     print(black_pegs, white_pegs)
    #     assert black_pegs == 4 and white_pegs == 0

    def test_resolve_game_ok7(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='wbwb', id=1), guess=Guess(code='bwbw',
                                                      game_id=1)).execute()

        self.assertEqual(black_pegs, 0)
        self.assertEqual(white_pegs, 4)

    def test_resolve_game_ok8(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='ooow', id=1),
            guess=Guess(code='owww',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 2)
        self.assertEqual(white_pegs, 0)

    def test_resolve_game_ok9(self):
        black_pegs, white_pegs = ResolveGame(
            game=Game(code='ooow', id=1),
            guess=Guess(code='OWWW',
                        game_id=1)).execute()

        self.assertEqual(black_pegs, 2)
        self.assertEqual(white_pegs, 0)

    def test_validate_code_ok1(self):
        self.assertTrue(ValidateCode(guess_code='owww').execute())

    def test_validate_code_ok2(self):
        self.assertTrue(ValidateCode(guess_code='rbrb').execute())

    def test_validate_code_ko1(self):
        self.assertFalse(ValidateCode(guess_code='xxxx').execute())

    def test_validate_code_ko2(self):
        self.assertFalse(ValidateCode(guess_code='defesafrfa').execute())

    def test_change_state_ok1(self):
        self.assertEqual(ChangeState(round_count=8, black_pegs=4, game=Game(
            code='rrbb')).execute().state, config.win_state)

    def test_change_state_ok2(self):
        self.assertEqual(ChangeState(round_count=8, black_pegs=3, game=Game(
            code='rrbb')).execute().state, config.on_state)

    def test_change_state_ok3(self):
        self.assertEqual(ChangeState(round_count=10, black_pegs=3, game=Game(
            code='rrbb')).execute().state, config.lose_state)

    def test_validate_code_upper_ok1(self):
        self.assertTrue(ValidateCodeLen(guess_code='rrgg').execute())

    def test_validate_code_upper_ok2(self):
        self.assertTrue(ValidateCodeLen(guess_code='RRGG').execute())

    def test_validate_code_upper_ko3(self):
        self.assertFalse(ValidateCodeLen(guess_code='rrrggg').execute())
