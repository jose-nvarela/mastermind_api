import unittest

from tests.utils.db import setup_database, create_game, create_current_game

from mastermind_api.infra.database.repositories.repositories import GuessRepo
from mastermind_api.infra.database.models import Game as GameModel
from mastermind_api.entities.guess_entity import Guess as GuessEntity


class TestGameRepository(unittest.TestCase):
    def setUp(self):
        self.session = setup_database()

    def test_create_guess_ok1(self):
        game: GameModel = create_game(self.session)

        guess_ent = GuessEntity(code='xxxx', game_id=game.id)
        guess = GuessRepo(self.session).create_guess(guess=guess_ent)

        self.assertEqual(guess.code, 'xxxx')

    def test_get_guess_by_gameid_ok1(self):
        game = create_current_game(self.session)
        guess = GuessRepo(self.session).get_guess_by_gameid(game_id=game.id)

        self.assertEqual(len(guess), 2)
