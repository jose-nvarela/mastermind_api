from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import MetaData

from mastermind_api.infra.database.models import Base, Game, Guess


def setup_database():
    engine = create_engine('sqlite:///:memory:')
    MetaData().create_all(engine)
    connection = engine.connect()
    Base.metadata.bind = connection
    Base.metadata.create_all()
    session = sessionmaker(bind=engine)()

    return session


def create_game(session):
    game = Game(code='rrgg', state='on')
    session.add(game)
    session.flush()
    session.commit()

    return game


def create_current_game(session) -> Game:
    game = Game(code='rrgg', state='on')
    session.add(game)
    session.flush()
    g1 = Guess(game_id=game.id, code='rrgb')
    g2 = Guess(game_id=game.id, code='rbgb')
    session.add(g1)
    session.add(g2)
    session.commit()

    return game
