from mastermind_api.config.settings import Settings


def get_config():
    return Settings(db_engine_url='fakeurl', log_level='fatal')
