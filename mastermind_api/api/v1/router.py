from fastapi import APIRouter

from mastermind_api.api.v1.endpoints import health, game, guess


v1_router = APIRouter()
v1_router.include_router(
    health.router, prefix='/health', tags=['Health-check'])
v1_router.include_router(game.router, prefix='/game', tags=['Game'])
v1_router.include_router(guess.router, prefix='/guess', tags=['Guess'])
