from fastapi import APIRouter
from pydantic import validator
from fastapi.exceptions import HTTPException

from mastermind_api.config.settings import config
from mastermind_api.entities import Game, Guess
from mastermind_api.infra.database.repositories.repositories import (
    GameRepo, GuessRepo)
from mastermind_api.infra.database.orm import (db)
from mastermind_api.use_cases.validate_code import (
    ValidateCode, ValidateCodeLen)

router = APIRouter()


class CurrentGameResponse(Game):
    guess: list[Guess]
    """Response model validation"""


class GameRequest(Game):
    class Config:
        schema_extra = {
            'example':
                {
                    'code': 'rrgg',
                }
        }

    @validator('code')
    def code_with_correct_colors(cls, v):
        if not ValidateCode(guess_code=v).execute():
            raise ValueError('Code not allowed')
        return v.title()

    @validator('code')
    def code_with_correct_len(cls, v):
        if not ValidateCodeLen(guess_code=v).execute():
            raise ValueError(
                'Code len not allowed, code len must be {}'.format(
                    config.code_len))
        return v.title()


@router.get(
    path='/',
    response_model=CurrentGameResponse
)
async def get_current_game(game_id: int) -> CurrentGameResponse:
    """ Get the current game with all guesses associated with its game."""
    # get current game
    game: Game = GameRepo(db).get_game(game_id=game_id)
    # get guess associated to its game
    guess_list: list[Guess] = GuessRepo(
        db).get_guess_by_gameid(game_id=game_id)

    if not game:
        raise HTTPException(404, detail="Game not found")
    return CurrentGameResponse(id=game.id,
                               state=game.state,
                               timestamp=game.timestamp,
                               code=game.code,
                               guess=guess_list)


@router.post(
    path='/',
    response_model=Game
)
async def create_game(game: GameRequest) -> Game:
    """ Create a  new game ."""
    # create new game
    # if ValidateCode(guess_code=game.code).execute():

    game_res: Game = GameRepo(db).create_game(Game(code=game.code, state="on"))

    return game_res
