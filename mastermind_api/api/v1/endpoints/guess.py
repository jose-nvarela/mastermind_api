from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from pydantic import validator

from mastermind_api.api import logger
from mastermind_api.infra.database.repositories.repositories import (
    GameRepo, GuessRepo)
from mastermind_api.config.settings import config
from mastermind_api.entities.guess_entity import Guess
from mastermind_api.entities.game_entity import Game
from mastermind_api.use_cases.validate_code import (
    ValidateCode, ValidateCodeLen)
from mastermind_api.use_cases.change_state import ChangeState
from mastermind_api.use_cases.resolve_game import ResolveGame
from mastermind_api.infra.database.orm import db

router = APIRouter()


class GuessRequest(Guess):
    class Config:
        schema_extra = {
            'example':
                {
                    'game_id': 1,
                    'code': 'rrgg',
                }
        }

    @validator('code')
    def code_with_correct_colors(cls, v):
        if not ValidateCode(guess_code=v).execute():
            raise ValueError('Code not allowed')
        return v.title()

    @validator('code')
    def code_with_correct_len(cls, v):
        if not ValidateCodeLen(guess_code=v).execute():
            raise ValueError(
                'Code len not allowed, code len must be {}'.format(
                    config.code_len))
        return v.title()


class GuessResponse(Guess):
    """Response model validation"""


@router.post(
    path='/',
    response_model=GuessResponse
)
async def create_guess(guess_req: GuessRequest) -> GuessResponse:
    """ Get the current game with all guesses associated with its game."""

    # get game
    game: Game = GameRepo(db, auto_commit=False).get_game(
        game_id=guess_req.game_id)

    if game.state != config.on_state:
        raise HTTPException(400, detail="The game is finished")
    try:
        round_count: int = len(
            GuessRepo(db, auto_commit=False).get_guess_by_gameid(
                game_id=game.id))

        # resolve game
        black_pegs_resol,  white_pegs_resol = ResolveGame(
            game=game, guess=guess_req).execute()
        # prepare to save guess into db
        guess: Guess = Guess(code=guess_req.code,
                             black_pegs=black_pegs_resol,
                             white_pegs=white_pegs_resol,
                             game_id=game.id)
        # save guess in db
        guess = GuessRepo(db, auto_commit=False).create_guess(guess=guess)

        game: Game = ChangeState(round_count=round_count,
                                 black_pegs=black_pegs_resol,
                                 game=game).execute()
        # save game changes
        if game:
            GameRepo(db, auto_commit=False).update_game(game=game)
    except Exception as e:
        logger.error('Unexpected exception in create guess: {}'.format(e))
        logger.debug('appliying rollback')
        db.rollback()
        raise e

    db.commit()
    return GuessResponse(id=guess.id,
                         timestamp=guess.timestamp,
                         game_id=guess.game_id,
                         code=guess.code,
                         black_pegs=guess.black_pegs,
                         white_pegs=guess.white_pegs)
