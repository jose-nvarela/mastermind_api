from fastapi import FastAPI
import uvicorn

from mastermind_api.config.settings import config
from mastermind_api.api.v1.router import v1_router

from mastermind_api.version import VERSION

app = FastAPI(
    version=VERSION,
    title=config.app_name,
    openapi_url=f'{config.api_prefix}/openapi.json')

app.include_router(v1_router, prefix=config.api_prefix)


if __name__ == '__main__':
    uvicorn.run('main:app', host='0.0.0.0', port=5000, reload=True)
