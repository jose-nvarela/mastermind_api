import logging
from logging.config import dictConfig

from mastermind_api.entities.game_entity import Game # noqa
from mastermind_api.entities.guess_entity import Guess # noqa 
from mastermind_api.config.settings import LogConfig

dictConfig(LogConfig().dict())
logger = logging.getLogger("entities")
