from datetime import datetime

from pydantic import BaseModel


class Game(BaseModel):
    code: str
    state: str = "on"
    timestamp: datetime = None
    id: int = None

    def __repr__(self):
        return "id:{},state:{}, code:{}, timestamp:{}".format(
            self.id, self.state, self.code, self.timestamp)
