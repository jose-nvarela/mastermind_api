from datetime import datetime

from pydantic import BaseModel


class Guess(BaseModel):
    game_id: int
    code: str
    black_pegs: int = None
    white_pegs: int = None
    timestamp: datetime = None
    id: int = None

    def __repr__(self):
        return "id:{},\
                game_id: {},\
                code:{},\
                timestamp:{},\
                bpegs: {}, \
                wpegs: {}".format(
            self.id,
            self.game_id,
            self.code,
            self.timestamp,
               self.black_pegs,
            self.white_pegs)
