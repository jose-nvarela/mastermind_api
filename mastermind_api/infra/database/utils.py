from sqlalchemy.sql import expression
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import DateTime


class utcnow(expression.FunctionElement):
    type = DateTime()
    inherit_cache = True

# Custom method for current timestamp


@compiles(utcnow, 'postgresql')
def pg_utcnow(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"

# Custom method for current timestamp


@compiles(utcnow, 'sqlite')
def pg_utcnow(element, compiler, **kw):  # noqa
    return "CURRENT_TIMESTAMP"
