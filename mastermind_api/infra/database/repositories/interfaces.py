from abc import ABCMeta, abstractmethod

from mastermind_api.entities.game_entity import Game as GameEntity
from mastermind_api.entities.guess_entity import Guess as GuessEntity


class GameInterface(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, session, auto_commit=True):
        raise NotImplementedError

    @abstractmethod
    def create_game(self, game: GameEntity) -> GameEntity:
        """Create a game item into DB

        Args:
            game (GameEntity):

        Raises:
            SQLAlchemyError: Db operations error
            Exception: unspected error

        Returns:
            GameEntity: The same object,
            with more db dependible data like id or timestamp
        """
        raise NotImplementedError

    @abstractmethod
    def get_game(self, game: GameEntity) -> GameEntity:
        """Get a current game with game info

        Args:
            game (GameEntity):

        Raises:
            SQLAlchemyError: Db operations error
            Exception: unspected error

        Returns:
            GameEntity: game object
        """
        raise NotImplementedError

    @abstractmethod
    def update_game(self, game: GameEntity) -> GameEntity:
        """update a gem

        Args:
            game (GameEntity):

        Raises:
            SQLAlchemyError: Db operations error
            Exception: unspected error

        Returns:
             GameEntity: game object
        """
        raise NotImplementedError


class GuessInterface(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, session, auto_commit=True):
        raise NotImplementedError

    @abstractmethod
    def create_guess(self, guess: GuessEntity) -> GuessEntity:
        """Create a guess item into DB

        Args:
            game (GameEntity):

        Raises:
            SQLAlchemyError: Db operations error
            Exception: unspected error

        Returns:
            GameEntity: The same object,
            with more db dependible data like id or timestamp
        """
        raise NotImplementedError

    @abstractmethod
    def get_guess_by_gameid(self, game: GameEntity) -> list[GuessEntity]:
        """get a guess items by gameid

        Args:
            game (GameEntity):

        Raises:
            SQLAlchemyError: Db operations error
            Exception: unspected error

        Returns:
            [GuessEntity]: list of guess
        """
        raise NotImplementedError
