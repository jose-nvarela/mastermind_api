from sqlalchemy import exc

from mastermind_api.infra.database import logger
from mastermind_api.infra.database.repositories.interfaces import (
    GameInterface, GuessInterface)

from mastermind_api.infra.database.models import (
    Game as GameModel, Guess as GuessModel)
from mastermind_api.entities.game_entity import Game as GameEntity
from mastermind_api.entities.guess_entity import Guess as GuessEntity


class GameRepo(GameInterface):
    def __init__(self, session, auto_commit=True):
        self.session = session
        self.auto_commit = auto_commit

    def create_game(self, game: GameEntity) -> GameEntity:
        try:
            logger.debug("Init create game with data: {}".format(game))
            game_model = GameModel(code=game.code, state=game.state)
            self.session.add(game_model)
            self.session.flush()
            if self.auto_commit:
                self.session.commit()
            logger.debug("Created game with id: {}".format(game.id))
        except exc.SQLAlchemyError as e:
            logger.error("SQLalchemy exception: {}".format(e))
            raise e
        except Exception as e:
            logger.error("Unspected error: {}".format(e))
            raise e

        return GameEntity(id=game_model.id,
                          code=game_model.code,
                          state=game_model.state,
                          timestamp=game_model.timestamp)

    def get_game(self, game_id: int) -> GameEntity:
        try:
            logger.debug(
                "Init get game from db, with gameid: {}".format(game_id))
            game: GameModel = self.session.query(
                GameModel).filter_by(id=game_id).first()

        except exc.SQLAlchemyError as e:
            logger.error("SQLalchemy exception: {}".format(e))
            raise e
        except Exception as e:
            logger.error("Unspected error: {}".format(e))
            raise e
        return game

    def update_game(self, game: GameEntity) -> GameEntity:
        try:
            logger.debug(
                "Init update game, with gameid: {}".format(game.id))
            self.session.query(
                GameModel).filter_by(id=game.id).update({'state': game.state})
            if self.auto_commit:
                self.session.commit()
        except exc.SQLAlchemyError as e:
            logger.error("SQLalchemy exception: {}".format(e))
            raise e
        except Exception as e:
            logger.error("Unspected error: {}".format(e))
            raise e

        return game


class GuessRepo(GuessInterface):
    def __init__(self, session, auto_commit=True):
        self.session = session
        self.auto_commit = auto_commit

    def get_guess_by_gameid(self, game_id) -> list[GuessEntity]:
        try:
            logger.debug(
                "Init get  guess by game id , with gameid: {}".format(game_id))
            guess: GuessModel = self.session.query(
                GuessModel).filter_by(game_id=game_id).all()
            guess_list: list[GuessEntity] = []

            for g in guess:
                guess_list.append(GuessEntity(id=g.id,
                                              code=g.code,
                                              timestamp=g.timestamp,
                                              game_id=g.game_id,
                                              black_pegs=g.black_pegs,
                                              white_pegs=g.white_pegs))
        except exc.SQLAlchemyError as e:
            logger.error("SQLalchemy exception: {}".format(e))
            raise e
        except Exception as e:
            logger.error("Unspected error: {}".format(e))
            raise e

        return guess_list

    def create_guess(self, guess: GuessEntity) -> GuessEntity:
        try:
            logger.debug("Init create guess, with data: {}".format(guess))
            guess_model = GuessModel(code=guess.code,
                                     game_id=guess.game_id,
                                     black_pegs=guess.black_pegs,
                                     white_pegs=guess.white_pegs)
            self.session.add(guess_model)
            self.session.flush()
            if self.auto_commit:
                self.session.commit()
            logger.debug("Created guess with id: {}".format(guess.id))
        except exc.SQLAlchemyError as e:
            logger.error("SQLalchemy exception: {}".format(e))
            raise e
        except Exception as e:
            logger.error("Unspected error: {}".format(e))
            raise e

        return GuessEntity(id=guess_model.id,
                           code=guess_model.code,
                           game_id=guess_model.game_id,
                           timestamp=guess_model.timestamp,
                           black_pegs=guess.black_pegs,
                           white_pegs=guess.white_pegs)
