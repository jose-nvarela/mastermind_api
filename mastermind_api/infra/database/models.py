from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, String, DateTime, Integer, ForeignKey

from mastermind_api.infra.database.utils import utcnow


Base = declarative_base()


class Guess(Base):
    __tablename__ = 'guess'

    id = Column(Integer, primary_key=True, autoincrement=True)
    game_id = Column(Integer, ForeignKey('game.id'))
    code = Column(String)
    timestamp = Column(DateTime, server_default=utcnow())
    black_pegs = Column(Integer)
    white_pegs = Column(Integer)


class Game(Base):
    __tablename__ = 'game'

    id = Column(Integer, primary_key=True, autoincrement=True)
    state = Column(String)
    code = Column(String)
    timestamp = Column(DateTime, server_default=utcnow())
    game = relationship(Guess)
