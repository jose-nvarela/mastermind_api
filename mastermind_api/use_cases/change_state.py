from mastermind_api.use_cases import logger
from mastermind_api.config.settings import config
from mastermind_api.entities.game_entity import Game


class ChangeState():
    def __init__(self, round_count: int, black_pegs: int, game: Game):
        """
        Args:
            round_count (int): current round counter of game
            black_pegs (int): back pegs for a current game
            game (Game): game entity
        """
        self.round_count: int = round_count
        self.black_pegs: int = black_pegs
        self.game: Game = game

    def execute(self) -> Game:
        """
        Change the state of a game, the state could be on, win or lose
        Returns:
            Game: game entity with state updated
        """
        logger.info("triying to change game {} state".format(self.game.id))
        if self.black_pegs == config.code_len:
            # the player wins the game!
            logger.info("the player wins the game!")
            self.game.state = config.win_state
        elif self.round_count >= config.rounds:
            # the player loses the game :(
            logger.info("the player loses the game :(")
            self.game.state = config.lose_state

        return self.game
