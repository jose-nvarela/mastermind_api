from mastermind_api.use_cases import logger
from mastermind_api.entities import Game, Guess


class ResolveGame():
    def __init__(self, game: Game, guess: Guess):
        """
        Args:
            game (Game): Game entity
            guess (Guess): Guess entity
        """
        self.game = game
        self.guess = guess

    def execute(self) -> tuple[int, int]:
        """ Given game code and guess code,
        resolve the came given black pegs or white pegs.

        Returns:
            tuple(int, int): 0: black pegs, 1: white pegs
        """
        logger.info("Resolving game: {} with guess data: {}".format(
            self.game, self.guess))
        black_pegs: int = 0
        white_pegs: int = 0

        game_code: list = list(self.game.code.upper())
        guess_code: list = list(self.guess.code.upper())

        for i in range(len(self.game.code.upper())):
            if guess_code[i] and guess_code[i] == game_code[i]:
                black_pegs += 1
                game_code[i] = None
                guess_code[i] = None

        for i in range(len(game_code)):
            if guess_code[i] and guess_code[i] in game_code:
                white_pegs += 1
        logger.info("Game {} resolved with result: black_pegs={},\
                    white_pegs={} ".format(
            self.game.id, black_pegs, white_pegs))

        return black_pegs, white_pegs
