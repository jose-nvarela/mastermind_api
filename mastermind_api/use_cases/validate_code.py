from mastermind_api.use_cases import logger
from mastermind_api.config.settings import config


class ValidateCode():
    def __init__(self, guess_code: str):
        """
        Args:
            guess_code (str): guess_code
        """
        self.guess_code = guess_code.upper()
        self.allow_colors = self.allow_colors = config.allow_colors.upper()

    def execute(self) -> bool:
        """ Return true or false if all the color codes are in a list of allowed colors

        Returns:
            bool: true: code ok, false code ko
        """
        logger.info("Validating code: {} ".format(self.guess_code))
        not_allow_codes: list = [
            x for x in self.guess_code if x not in self.allow_colors]

        return True if not not_allow_codes else False


class ValidateCodeLen():
    def __init__(self, guess_code: str):
        """
        Args:
            guess_code (str): guess_code
        """
        self.guess_code = guess_code

    def execute(self) -> int:
        """ Return length of a code

        Returns:
            bool: true: code ok, false code ko
        """
        logger.info("Validating code len: {} ".format(self.guess_code))

        return len(self.guess_code) == config.code_len
