# Mastermind API

API of the logic game called mastermind.

## Installation

docker-compose -f docker-compose/docker-compose.yaml  up

## Swagger Documentation

http://0.0.0.0/docs


## Run tests with coverage
- pip install poetry
- poetry install
- coverage run -m unittest discover -p 'test_*.py' tests && coverage report
